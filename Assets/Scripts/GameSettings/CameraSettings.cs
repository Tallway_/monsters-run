using Cinemachine;
using UnityEngine;

public class CameraSettings : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _camera;
    [SerializeField] private CameraMover _followingObject;
    private float _lensFieldOfViewInLandscape, _lensFieldOfViewInPortrait;
    private ScreenOrientation _tempOrientation;

    private void OnEnable() 
    {
        GameController.Instance.GameEnding += OnGameEnding;
    }

    private void OnDisable() 
    {
        GameController.Instance.GameEnding -= OnGameEnding;
    }

    private void OnGameEnding()
    {
        _camera.m_Follow = null;
    }

    private void Start()
    {
        Initizlize();
    }

    private void Initizlize()
    {
        Application.targetFrameRate = 120;

        #if UNITY_ANDROID
            _lensFieldOfViewInLandscape = 22f;
            _lensFieldOfViewInPortrait = 37f;
            _tempOrientation = Screen.orientation;
            SetLensFieldOfView();
        #endif

        #if UNITY_STANDALONE_WIN || UNITY_EDITOR
            _lensFieldOfViewInLandscape = 26f;
            _camera.m_Lens.FieldOfView = _lensFieldOfViewInLandscape;
        #endif

        _camera.m_Follow = _followingObject.transform;
    }

    #if UNITY_ANDROID
        private void Update() 
        {
            if(Screen.orientation != _tempOrientation)
            {
                _camera.m_Lens.FieldOfView = _lensFieldOfViewInLandscape;
                SetLensFieldOfView();
            }       
        }

        private void SetLensFieldOfView()
        {
            ScreenOrientation currentOrientation = Screen.orientation;
            switch(currentOrientation)
            {
                case ScreenOrientation.Portrait:
                case ScreenOrientation.PortraitUpsideDown:
                    _camera.m_Lens.FieldOfView = _lensFieldOfViewInPortrait;
                    break;
                case ScreenOrientation.LandscapeLeft:
                case ScreenOrientation.LandscapeRight:
                default:
                    _camera.m_Lens.FieldOfView = _lensFieldOfViewInLandscape;
                    break; 
            }

            _tempOrientation = currentOrientation;
        }
    #endif
}



