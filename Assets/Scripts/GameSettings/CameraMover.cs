using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CameraMover : MonoBehaviour
{
    #region Constants
    private const float OffsetToTarget = 30f; 
    private const float MinDistance = 3f;
    private const float MaxDistance = 8f;
    private const float StartCameraXPosition = -1f;
    private const float StartCameraZPosition = -2f;
    private const float CameraPositionOffset = 1f;
    private const float JumpSpeedCorrection = 0.01f;
    private const float CameraMovingOffset = 2f;
    private const float SmoothSpeed = 4f;
    #endregion

    [Range(0f, 10f)] public float StartSpeed;
    public bool IsGamePlaying { get; set; } 

    [SerializeField] private CharacterBehavior _character;
    [SerializeField] private Slider _distanceSlider;
    private Vector3 _targetPostion, _startPostion;
    private float _speed, _distanceToPlayer, _sliderValue;
    private bool _shouldNewMovementBeReassigned;

    private void OnEnable() 
    {
        CharacterMover.XPositionChanged += OnXPositionChanged;
        GameController.Instance.GameEnding += OnGameEnding;
    }

    private void OnDisable() 
    {
        CharacterMover.XPositionChanged -= OnXPositionChanged;
        GameController.Instance.GameEnding -= OnGameEnding;
    }

    private void OnGameEnding()
    {
        StopAllCoroutines();
    }

    private void OnXPositionChanged(float newXPosition)
    {
        Vector3 offsetedPosition = new Vector3(newXPosition, 0f, 0f);
        Vector3 nextPosition = transform.position + offsetedPosition;

        StartCoroutine(SmoothAppcoach(nextPosition));
  
        _targetPostion += offsetedPosition;
        _startPostion += offsetedPosition;
    }

    private IEnumerator SmoothAppcoach(Vector3 nextPosition)
    {
        Vector3 startPosition = transform.position;

        for(float i = 0; i < 1; i += Time.deltaTime * SmoothSpeed)
        {
            transform.position = Vector3.Lerp(startPosition, nextPosition, i);

            yield return null;
        }
    }

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        transform.position = new Vector3(StartCameraXPosition, 0, StartCameraZPosition);
        _startPostion = transform.position;
        _distanceToPlayer = GetDistanceTo(_character.transform.position);

        _shouldNewMovementBeReassigned = true;
        IsGamePlaying = false;
        
        SetSliderValue();
    }

    private void Update()
    {
        if(IsGamePlaying)
        {
            if (_shouldNewMovementBeReassigned)
            {
                SetTargetPosition();
                StartCoroutine(ObjectMovement());
            }

            CameraSpeedCorretion();

            SetSliderValue();

            if (_distanceToPlayer > MaxDistance)
            {
                AudioPlayer.Instance.AudioSource.PlayOneShot(AudioPlayer.Instance.DeathInIdleSound);
                _character.PlayDeathOutOfCameraAnimation();
                _character.IsDead = true;
                IsGamePlaying = false;
                StopAllCoroutines();
            }
        }
    }

    private void SetSliderValue()
    {
        _sliderValue = 1 - (_distanceToPlayer / MaxDistance);

        _distanceSlider.value = _sliderValue;
    }

    private void CameraSpeedCorretion()
    {
        _distanceToPlayer = GetDistanceTo(_character.transform.position);

        _speed = _distanceToPlayer < MinDistance ? _speed + (2f / _distanceToPlayer) : StartSpeed;
    }

    private IEnumerator ObjectMovement()
    {
        _shouldNewMovementBeReassigned = false;

        for(float i = 0; i < 1; i += Time.deltaTime * JumpSpeedCorrection * _speed)
        {
            transform.position = Vector3.Lerp(_startPostion, _targetPostion, i);

            yield return null;
        }

        _startPostion = transform.position;
        _shouldNewMovementBeReassigned = true;
    }

    private float GetDistanceTo(Vector3 objectPosition)
    {
        return transform.position.z - objectPosition.z;
    }

    private void SetTargetPosition()
    {
        _targetPostion = transform.position + Vector3.forward * OffsetToTarget;
    }
}
