using UnityEngine;

public class SceneBehavior : MonoBehaviour
{   
    [SerializeField] private DataOperator _dataOperator;

    private void OnApplicationQuit() 
    {
        _dataOperator.SaveData();
    }  
}
