using System;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private const int LinesAmountToUpdate = 10;

    public static GameController Instance { get { return _instance; } }
    public event Action GameUpdated;
    public event Action GameEnding;

    private static GameController _instance;
    [SerializeField] private Transform _playerSpawnPlace;
    [SerializeField] private CharacterBehavior _character;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } 
        else 
        {
            _instance = this;
        }
    }

    private void Start()
    {
        _character.transform.position = _playerSpawnPlace.position;  
    }
 
    private void Update()
    {
        if(_character.LinesPassedCount == LinesAmountToUpdate)
        {
            _character.LinesPassedCount = 0;
            GameUpdated?.Invoke();
        }  

        if(_character.IsDead)
        {
            GameEnding?.Invoke();
            _character.GetComponent<CharacterMover>().enabled = false;
        }      
    }
}
