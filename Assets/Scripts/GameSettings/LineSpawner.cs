using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineSpawner : MonoBehaviour
{
    #region Constants
    private const int StartLinesAmount = 30;
    private const int LinesUpdateAmount = 10;
    private const int StartLines= 5;
    private const float OffsetValue = 2f;
    #endregion

    [Header("List of spawning lines")]
    public List<GameObject> LinePrefabs;

    [Space, Header("Additional lines")]
    public GameObject AlternativeGroundLinePrefab;
    public GameObject RoadLineDefaultPrefab;
    public GameObject StartLinePrefab;
    
    [Space, Header("Start spawn position")]
    public Transform SpawnPoint;

    private Queue<GameObject> _createdLines;
    private bool _wasGroundLineSpawned, _wasRoadLineSpawned;


    private void OnEnable() => GameController.Instance.GameUpdated += OnGameUpdated;

    private void OnDisable() => GameController.Instance.GameUpdated -= OnGameUpdated;

    private void OnGameUpdated()
    {
        StartCoroutine(CreateLines());      
    }

    private IEnumerator CreateLines()
    {
        yield return DeleteLines();

        SpawnLines(LinesUpdateAmount);
    }

    private IEnumerator DeleteLines()
    {
        for(int i = 0; i < LinesUpdateAmount; i++)
        {
            Destroy(_createdLines.Dequeue());
        }

        yield return null; 
    }

    private void Start()
    {
        Initialize();
    }
    
    private void Initialize()
    {
        _wasGroundLineSpawned = false;
        _wasRoadLineSpawned = false;
        _createdLines = new Queue<GameObject>();

        SpawnInitialLines(); 
    }

    private void SpawnInitialLines()
    {
        GameObject groundLine = LinePrefabs.Find(line => line.CompareTag(Tags.GroundLine));

        for(int i = 0; i < StartLines; i++)
        {
            SpawnLine(StartLinePrefab);
        }

        for(int i = 0; i < StartLines; i++)
        {
            SpawnGroundLine(groundLine);
        }

        SpawnLines(StartLinesAmount);
    }

    private void SpawnLines(int count)
    {
        for(int currentNumberOfLines = 0; currentNumberOfLines <= count; currentNumberOfLines++)
        {
            GameObject selectedLine = LinePrefabs[Random.Range(0,LinePrefabs.Count)];

            switch (selectedLine.tag)
            {
                case Tags.GroundLine: 
                    currentNumberOfLines += CheckIsRoadLineSpawned();
                    if(currentNumberOfLines == count) 
                    {
                        return;
                    }
                    SpawnGroundLine(selectedLine);
                    break;

                case Tags.RoadLine: 
                    SpawnRoadLine(selectedLine);
                    break;

                case Tags.RiverLine:
                default:
                    currentNumberOfLines += CheckIsRoadLineSpawned();
                    if(currentNumberOfLines == count) 
                    {
                        return;
                    }
                    SpawnLine(selectedLine);
                    break;
            }         
        }
    }

    private void SpawnLine(GameObject linePrefab)
    {
        GameObject line = Instantiate(linePrefab, gameObject.transform);
        line.transform.position = SpawnPoint.position;

        SpawnPoint.position += Vector3.forward * OffsetValue; 

        _createdLines.Enqueue(line);
    }

    private void SpawnGroundLine(GameObject groundLine)
    {
        if(_wasGroundLineSpawned)
        {
            _wasGroundLineSpawned = false;
            SpawnLine(AlternativeGroundLinePrefab);
            return;
        }

        _wasGroundLineSpawned = true;
        SpawnLine(groundLine);
    }

    private void SpawnRoadLine(GameObject roadLine)
    {
        _wasRoadLineSpawned = true;
        SpawnLine(roadLine);
    }

    private int CheckIsRoadLineSpawned()
    {
        if(_wasRoadLineSpawned) 
        {
            _wasRoadLineSpawned = false;
            SpawnLine(RoadLineDefaultPrefab);
            return 1;
        }
        return 0;
    }
}
