using UnityEngine;

public class EndLine : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out VehicleBehavior vehicle))
        {
            vehicle.ReturnToPool();
        }
    }

    private void OnTriggerStay(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out LogBehavior log))
        {
            log.ReturnToPool();
        }     
    }
}
