using System.Collections;
using UnityEngine;

public class StartGameZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out CharacterBehavior character))
        {
            StartCoroutine(Disable(character));
            AudioPlayer.Instance.AudioSource.PlayOneShot(AudioPlayer.Instance.DeathInBoundSound);
            character.IsDead = true;
        }      
    }

    private void OnTriggerExit(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out LogBehavior log))
        {
            log.gameObject.GetComponent<Rigidbody>().velocity = log.InGameZoneVelocity;
        }
    }

        private IEnumerator Disable(CharacterBehavior character)
    {
        yield return new WaitForSeconds(2.5f);

        character.gameObject.SetActive(false);
    }
}
