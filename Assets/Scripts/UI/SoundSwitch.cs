using System.Collections;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class SoundSwitch : SettingButton
{
    private const string MasterVolume = nameof(MasterVolume);

    [SerializeField] private AudioMixerGroup _soundMixer;

    private void Awake()
    {
        _buttonImage = GetComponent<Image>();
    }

    private void Start() 
    {
        if(_isOn)
        {
            StartCoroutine(SetApplicationStart()); 
        }  
    }

    public override void ChangeOption()
    {
        if(_isOn)
        {
            _buttonImage.sprite = _offFunctionSprite;
            _soundMixer.audioMixer.SetFloat(MasterVolume, MinVolumeValue);

            _isOn = false;
        }
        else
        {
            _buttonImage.sprite = _onFunctionSprite;
            _soundMixer.audioMixer.SetFloat(MasterVolume, MaxVolumeValue);

            _isOn = true;
        }   
    }

    public override int GetOption()
    {
        return _buttonImage.sprite == _onFunctionSprite ? 1 : 0;
    }

    public override void SetOption(int enabled)
    {
        _isOn = enabled == 1;

        if(_isOn)
        {
            _buttonImage.sprite = _onFunctionSprite;
            _soundMixer.audioMixer.SetFloat(MasterVolume, MaxVolumeValue);
        }
        else
        {
            _buttonImage.sprite = _offFunctionSprite;
            _soundMixer.audioMixer.SetFloat(MasterVolume, MinVolumeValue);
        }
    }

    private IEnumerator SetApplicationLoading()
    {
        for(float i = 0; i < 1; i += Time.deltaTime * 2f)
        {
            _soundMixer.audioMixer.SetFloat(MasterVolume, MinVolumeValue * i);

            yield return null;
        }

        _soundMixer.audioMixer.SetFloat(MasterVolume, MinVolumeValue);
    }

    private IEnumerator SetApplicationStart()
    {
        for(float i = 1; i > 0; i -= Time.deltaTime * 2f)
        {
            _soundMixer.audioMixer.SetFloat(MasterVolume, MinVolumeValue * i);

            yield return null;
        }

        _soundMixer.audioMixer.SetFloat(MasterVolume, MaxVolumeValue);
    }

    public void InvokeApplicationLoading()
    {
        StartCoroutine(SetApplicationLoading());
    }
}
