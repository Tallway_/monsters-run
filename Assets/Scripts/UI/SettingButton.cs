using UnityEngine;
using UnityEngine.UI;

public abstract class SettingButton : MonoBehaviour
{
    protected const float MaxVolumeValue = 0f;
    protected const float MinVolumeValue = -80f;

    [SerializeField] protected Sprite _onFunctionSprite;
    [SerializeField] protected Sprite _offFunctionSprite;
    [SerializeField] protected Image _buttonImage;
    protected bool _isOn;
    
    public abstract int GetOption();
    public abstract void SetOption(int enabled);
    public abstract void ChangeOption();
}
