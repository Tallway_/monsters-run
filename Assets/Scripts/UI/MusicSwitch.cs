using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class MusicSwitch : SettingButton
{
    private const string MusicVolume = nameof(MusicVolume);
    private const float MaxVolume = -10f;

    [SerializeField] private AudioMixerGroup _musicMixer; 

    private void Awake()
    {
        _buttonImage = GetComponent<Image>();
    }
    
    private void Start() 
    {
        SetValue();
    }
    
    public override void ChangeOption()
    {
        if(_isOn)
        {
            _buttonImage.sprite = _offFunctionSprite;
            _musicMixer.audioMixer.SetFloat(MusicVolume, MinVolumeValue);

            _isOn = false;
        }
        else
        {
            _buttonImage.sprite = _onFunctionSprite;
            _musicMixer.audioMixer.SetFloat(MusicVolume, MaxVolume);

            _isOn = true;
        }       
    }

    public override int GetOption()
    {
        return _buttonImage.sprite == _onFunctionSprite ? 1 : 0;
    }

    public override void SetOption(int enabled)
    {
        _isOn = enabled == 1;
    }

    private void SetValue()
    {
        if(_isOn)
        {
            _buttonImage.sprite = _onFunctionSprite;
            _musicMixer.audioMixer.SetFloat(MusicVolume, MaxVolume);
        }
        else
        {
            _buttonImage.sprite = _offFunctionSprite;
            _musicMixer.audioMixer.SetFloat(MusicVolume, MinVolumeValue);
        }
    }
}
