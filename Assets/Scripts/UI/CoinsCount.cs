using TMPro;
using UnityEngine;

public class CoinsCount : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _coins;

    private int _coinsCount;

    private void OnEnable() 
    {
        CharacterBehavior.CoinPickingUp += OnCoinPickingUp;
    }
    private void OnDisable() 
    {
        CharacterBehavior.CoinPickingUp -= OnCoinPickingUp;
    }

    private void OnCoinPickingUp()
    {
        _coinsCount = GetValue() + 1;
        _coins.text = _coinsCount.ToString();
    }

    public int GetValue()
    {
        return int.Parse(_coins.text);
    }

    public void SetValue(int value)
    {
        _coinsCount = value;
        _coins.text = _coinsCount.ToString();
    }
}
