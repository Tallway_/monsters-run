using UnityEngine;

public class LockSwitch : SettingButton
{
    public override void ChangeOption()
    {
        if(_isOn)
        {
            _buttonImage.sprite = _offFunctionSprite;

            EnableAutoRotation();

            _isOn = false;
        }
        else
        {
            _buttonImage.sprite = _onFunctionSprite;

            DisableAutoRotation();

            _isOn = true;
        }
    }

    public override int GetOption()
    {
        return _buttonImage.sprite == _onFunctionSprite ? 1 : 0;
    }

    public override void SetOption(int enabled)
    {
        _isOn = enabled == 1;

        if(_isOn)
        {
            _buttonImage.sprite = _onFunctionSprite;

            DisableAutoRotation();
        }
        else
        {
            _buttonImage.sprite = _offFunctionSprite;

            EnableAutoRotation();
        }
    }

    private void EnableAutoRotation()
    {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortraitUpsideDown = true;
        Screen.autorotateToPortrait = true; 
    }

    private void DisableAutoRotation()
    {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToPortrait = false; 
    }
}
