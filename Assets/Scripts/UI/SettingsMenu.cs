using UnityEngine;

[RequireComponent(typeof(Animator))]
public class SettingsMenu : MonoBehaviour
{
    private const string RollIn = nameof(RollIn);
    private const string RollOut = nameof(RollOut);

    private Animator _animator;
    private bool _isClicked;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _isClicked = false;
    }

    public void OnClick()
    {
        if(_isClicked)
        {
            _animator.SetTrigger(RollOut);
            _isClicked = false;
        }
        else
        {
            _animator.SetTrigger(RollIn);
            _isClicked = true;
        }
    }

    public void OnPlay()
    {
        if(_isClicked)
        {
            _animator.SetTrigger(RollOut);
            _isClicked = false;
        }
    }
}
