using System;
using UnityEngine;

public class QualitySwitch : SettingButton
{
    private const int LowSettings = 2;
    private const int HightSettings = 5;

    public override void ChangeOption()
    {
        if(_isOn)
        {
            _buttonImage.sprite = _offFunctionSprite;

            SetLowQuality();

            _isOn = false;
        }
        else
        {
            _buttonImage.sprite = _onFunctionSprite;

            SetHighQuality();

            _isOn = true;
        }
    }

    public override int GetOption()
    {
        return _buttonImage.sprite == _onFunctionSprite ? 1 : 0;
    }

    public override void SetOption(int enabled)
    {
        _isOn = enabled == 1;

        if(_isOn)
        {
            _buttonImage.sprite = _onFunctionSprite;

            SetHighQuality();
        }
        else
        {
            _buttonImage.sprite = _offFunctionSprite;

            SetLowQuality();
        }
    }

    private void SetLowQuality()
    {
        Application.targetFrameRate = 60;
        QualitySettings.SetQualityLevel(LowSettings);
    }

    private void SetHighQuality()
    {
        Application.targetFrameRate = 120;
        QualitySettings.SetQualityLevel(HightSettings);
    }
}
