using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Animator))]
public class UIBehavior : MonoBehaviour
{
    private const string Game = nameof(Game);

    [SerializeField] private Animator _animator;

    private void OnEnable() 
    {
        GameController.Instance.GameEnding += OnGameEnding;       
    }

    private void OnDisable() 
    {
        GameController.Instance.GameEnding -= OnGameEnding;
    }

    private void OnGameEnding()
    {
        _animator.SetTrigger(AnimatorVariables.UI.GameEnded);
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(Game);
    }

    public void PauseTheGame()
    {
        Time.timeScale = 0;
    }

    public void ResumeTheGame()
    {
        Time.timeScale = 1;
    }
}
