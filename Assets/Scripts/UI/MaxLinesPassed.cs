using TMPro;
using UnityEngine;

public class MaxLinesPassed : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _maxLinesPassedCount;
    [SerializeField] private LinesPassed _linesPassed; 

    public int MaxLinesPassedNumber { get; set; }

    private void OnEnable() 
    {
        CharacterMover.LineChanged += OnLineChanged;   
    }

    private void OnDisable() 
    {
        CharacterMover.LineChanged += OnLineChanged;      
    }

    private void OnLineChanged(int value)
    {
        if(value > 0 && _linesPassed.CurrentLinesPassedCount >= MaxLinesPassedNumber)
        {
            AddValue(value);
        }
    }
    private void AddValue(int value)
    {
        MaxLinesPassedNumber += value;
        _maxLinesPassedCount.text = MaxLinesPassedNumber.ToString();
    }

    public int GetValue()
    {
        return int.Parse(_maxLinesPassedCount.text);
    }

    public void SetValue(int value)
    {
        MaxLinesPassedNumber = value;
        _maxLinesPassedCount.text = MaxLinesPassedNumber.ToString();
    }
}
