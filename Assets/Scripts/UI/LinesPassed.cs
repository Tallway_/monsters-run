using TMPro;
using UnityEngine;

public class LinesPassed : MonoBehaviour
{
    public int CurrentLinesPassedCount { get; private set; }

    [SerializeField] private TextMeshProUGUI _linesPassedCount; 

    private void OnEnable() 
    {
        CharacterMover.LineChanged += OnLineChanged;    
    }

    private void OnDisable() 
    {
        CharacterMover.LineChanged += OnLineChanged;       
    }

    private void OnLineChanged(int value)
    {
        if(value > 0 && CurrentLinesPassedCount == int.Parse(_linesPassedCount.text))
        {
            AddValue(value);
        }

        CurrentLinesPassedCount += value;
    }

    private void AddValue(int value)
    {
        int linesPassedCount = int.Parse(_linesPassedCount.text);
        linesPassedCount += value;
        _linesPassedCount.text = linesPassedCount.ToString();
    }

    private void Start()
    {
        CurrentLinesPassedCount = 0;
        _linesPassedCount.text = CurrentLinesPassedCount.ToString();
    }
}
