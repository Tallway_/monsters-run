using System.Collections.Generic;
using UnityEngine;

public class Pooler<T> : MonoBehaviour
    where T: MonoBehaviour
{
    private const int AmountToPool = 5;

    [Header("Pool of Prefabs"),SerializeField] private T[] _objectsToPool;
    private readonly Queue<T> _objectsPool = new Queue<T>();
    private Vector3 _rotation;
    private Vector3 _velocity;
    private Transform _parent;

    public void Init(Vector3 rotation, Vector3 velocity, Transform parent)
    {
        _rotation = rotation;
        _velocity = velocity;
        _parent = parent;
        
        for(int i = 0; i < AmountToPool; i++)
        {
            _objectsPool.Enqueue(CreateObject(_parent));
        }
    }

    private T CreateObject(Transform parent)
    {
        int randomObject = Random.Range(0, _objectsToPool.Length);

        T someObject = Instantiate(_objectsToPool[randomObject], _parent);
        someObject.gameObject.SetActive(false);
        someObject.gameObject.transform.Rotate(_rotation);

        return someObject;      
    }

    public T GetObject(Vector3 position)
    {
        T someObject = _objectsPool.Count == 0 ? CreateObject(_parent) : _objectsPool.Dequeue();

        someObject.gameObject.SetActive(true);
        someObject.gameObject.transform.position = position;
        someObject.gameObject.GetComponent<Rigidbody>().velocity = _velocity;
        
        return someObject;
    }

    public void ReturnObject(T someObject, Vector3 returnPosition)
    {
        someObject.transform.position = returnPosition;
        someObject.gameObject.SetActive(false);
        _objectsPool.Enqueue(someObject);
    }
}
