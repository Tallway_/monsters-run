using UnityEngine;

public class Line : MonoBehaviour
{
    [Header("Initial points for spawning")]
    public GameObject StartOfLine;
    public GameObject EndOfLine;
    public GameObject StartGameZone;
    public GameObject EndGameZone;

    protected virtual void SpawnStartMovableObjects() {}

    protected virtual Vector3[] GetSpawnPositionsForMovableObjects(int objectsCount, float multiplier, float randomXPosition) 
    { 
        Vector3[] spawnPositions = new Vector3[objectsCount];

        for(int i = 0; i < objectsCount; i++)
        {
            spawnPositions[i] = new Vector3(randomXPosition + i * multiplier, StartOfLine.transform.position.y, StartOfLine.transform.position.z);
        }

        return spawnPositions;
    }
}
