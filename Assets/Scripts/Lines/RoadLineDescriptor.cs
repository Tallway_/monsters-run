using System.Collections;
using UnityEngine;

public class RoadLineDescriptor : Line
{
    #region Constants
    private const int MinVeliclesAmount = 1;
    private const int MaxVehiclesAmount = 3;
    private const float MinTimerToSpawn = 4f;
    private const float MaxTimerToSpawn = 7f;
    private const float RangeOfSpawning = 20f;
    private const float Multiplier = 10f;
    private const float StartSpeed = 3f;
    private const float Offset = 2f;
    #endregion

    [Header("Pool of Vehicles"),SerializeField] private VehiclePooler _vehiclePooler;
    [Space, Header("Coin Prefab"), SerializeField] private Coin _coin;

    private float  _timer;
    private Vector3 _rotation;
    private Vector3 _startVehicleVelocity;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _timer = Random.Range(MinTimerToSpawn, MaxTimerToSpawn);
        _rotation = new Vector3(0, 0, 0);
        _startVehicleVelocity = -Vector3.right * StartSpeed;

        TrySwapStartPositions();

        _vehiclePooler.Init(_rotation, _startVehicleVelocity, transform);

        TryToSpawCoins();
        SpawnStartMovableObjects();

        StartCoroutine(SpawnVehicle(StartOfLine.transform.position, _timer));
    }

    private void TrySwapStartPositions()
    {
        float randomChance = Random.Range(0f, 1f);
        float chanceToReverseLine = 0.5f;

        if(randomChance > chanceToReverseLine)
        {
            SwapStartPositions();
            _rotation = new Vector3(0, 180, 0);
            _startVehicleVelocity = -_startVehicleVelocity;
        }
    }

    private void SwapStartPositions()
    {
        Vector3 temporaryPosition = StartOfLine.transform.position; 
        StartOfLine.transform.position = EndOfLine.transform.position;
        EndOfLine.transform.position = temporaryPosition;
    }

    protected override void SpawnStartMovableObjects()
    {
        int startVehiclesCount = Random.Range(MinVeliclesAmount, MaxVehiclesAmount);
        float randomXPosition = Random.Range(-RangeOfSpawning, RangeOfSpawning / Multiplier);
        Vector3[] spawningPositions = GetSpawnPositionsForMovableObjects(startVehiclesCount, Multiplier, randomXPosition);

        for(int i = 0; i < startVehiclesCount; i++)
        {
            SpawnVehicle(spawningPositions[i]);
        }
    }

    private IEnumerator SpawnVehicle(Vector3 spawnPosition, float pauseTime)
    {
        while(true)
        {
            SpawnVehicle(spawnPosition);
            yield return new WaitForSeconds(pauseTime);
        }
    }

    private void SpawnVehicle(Vector3 spawnPosition)
    {
        VehicleBehavior vehicle = _vehiclePooler.GetObject(spawnPosition);
        vehicle.Init(_vehiclePooler, StartOfLine.transform.position);
    }

    private void TryToSpawCoins()
    {
        Vector3 currentSpawnPosition = StartGameZone.transform.position - Vector3.right;

        while(currentSpawnPosition.x > EndGameZone.transform.position.x)
        {
            _coin.TryToSpawnCoin(currentSpawnPosition, this.gameObject.transform);
            currentSpawnPosition.x -= Offset; 
        }
    }
}
