using System.Collections;
using UnityEngine;

public class RiverLineDescriptor : Line
{
    #region Constants
    private const int MinLogsAmount = 1;
    private const int MaxLogsAmount = 4;
    private const float MinTimerToSpawn = 2f;
    private const float MaxTimerToSpawn = 5f;
    private const float StartSpeed = 10f;
    private const float Multiplier = 8f;
    private const float RangeOfSpawning = 16f;
    #endregion

    [Header("Log speed limits")]
    [Range(1f,10f)] public float minLogSpeed;
    [Range(2f,10f)] public float maxLogSpeed;

    [Header("Pool of Logs"), SerializeField] private LogPooler _logPooler;
    private float _inGameZoneSpeed;
    private float  _timer;
    private Vector3 _rotation;
    private Vector3 _startLogVelocity;
    private Vector3 _inGameZoneLogVelocity;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _inGameZoneSpeed = Random.Range(minLogSpeed, maxLogSpeed);
        _timer = Random.Range(MinTimerToSpawn, MaxTimerToSpawn);
        _rotation = new Vector3(0, 0, 0);
        _startLogVelocity = -Vector3.right * StartSpeed;
        _inGameZoneLogVelocity = -Vector3.right * _inGameZoneSpeed;

        TrySwapStartPositions();

        _logPooler.Init(_rotation, _startLogVelocity, transform);

        SpawnStartMovableObjects();

        StartCoroutine(SpawnLog(StartOfLine.transform.position, _timer));
    }

    private void TrySwapStartPositions()
    {
        float randomChance = Random.Range(0f, 1f);
        float chanceToReverseLine = 0.5f;

        if(randomChance > chanceToReverseLine)
        {
            SwapStartPositions();
            _startLogVelocity = -_startLogVelocity;
            _inGameZoneLogVelocity = -_inGameZoneLogVelocity;
        }
    }

    private void SwapStartPositions()
    {
        Swap(StartOfLine, EndOfLine);
        Swap(StartGameZone, EndGameZone);
    }

    private void Swap(GameObject first, GameObject second)
    {
        Vector3 temporaryPosition = first.transform.position;
        first.transform.position = second.transform.position;
        second.transform.position = temporaryPosition;
    }

    protected override void SpawnStartMovableObjects()
    {
        int startLogsCount = Random.Range(MinLogsAmount, MaxLogsAmount);
        float randomXPosition = Random.Range(-RangeOfSpawning, 0);
        Vector3[] spawningPositions = GetSpawnPositionsForMovableObjects(startLogsCount, Multiplier, randomXPosition);

        for(int i = 0; i < startLogsCount; i++)
        {
           SpawnLog(spawningPositions[i]);
        }        
    }

    private IEnumerator SpawnLog(Vector3 spawnPosition, float pauseTime)
    {   
        yield return new WaitForSeconds(Random.Range(0.5f,1f));

        while(true)
        {
            SpawnLog(spawnPosition);
            yield return new WaitForSeconds(pauseTime);
        }     
    }

    private void SpawnLog(Vector3 spawnPosition)
    {
        LogBehavior log = _logPooler.GetObject(spawnPosition);
        log.Initialize(_logPooler, StartOfLine.transform.position, _startLogVelocity, _inGameZoneLogVelocity);
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.TryGetComponent(out CharacterBehavior character))
        {
            character.PlayDeathInWaterAnimation();
            character.PlaySplashingWaterEffect();
            character.IsDead = true;

            AudioPlayer.Instance.AudioSource.PlayOneShot(AudioPlayer.Instance.DeathInRiverSound);
            character.gameObject.layer = 0;
        }
    }
}
