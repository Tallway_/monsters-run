using UnityEngine;

public class StartLineDescriptor : MonoBehaviour
{
    private const float PositionOffset = 2f;

    [SerializeField] private Transform StartLine;
    [SerializeField] private Transform EndLine;
    [Space, SerializeField] private GameObject[] StaticEnvironment;
   
    private void Start()
    {
        Spawn();        
    }

    private void Spawn()
    {
        Vector3 currentPosition = StartLine.position;

        while(currentPosition.x <= EndLine.position.x)
        {
            GameObject randomPieceOfEnvironment = StaticEnvironment[Random.Range(0, StaticEnvironment.Length)];
            Instantiate(randomPieceOfEnvironment, currentPosition, Quaternion.identity, transform);
            currentPosition.x += PositionOffset;
        }
    }
}
