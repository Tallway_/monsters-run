using UnityEngine;

public class GroundLineDescriptor : Line
{
    #region Constants
    private const float ChanceToSpawnInGameZone = 0.15f;
    private const float ChanceToSpawnOutGameZone = 0.6f;
    private const float MinChanceToSpawn = 0f;
    private const float MaxChanceToSpawn = 1f;
    private const float SpawningOffset = 2f;
    #endregion

    [Space, Header("Static Environment of Ground"), SerializeField] 
    public GameObject[] StaticEnvironment;
    
    [Space, Header("Coin Prefab"), SerializeField] 
    public GameObject CoinPrefab;

    private Vector3 _currentPosition;
   
    private void Start()
    { 
        SpawnEnvironmentObjects();
    }

    private void SpawnEnvironmentObjects()
    {
        _currentPosition = StartOfLine.transform.position;

        while(_currentPosition.x >= EndOfLine.transform.position.x)
        {
            if(IsSpawnPositionInGameZone())
            {
                TryToSpawnPieceOfEnvironment(_currentPosition, ChanceToSpawnInGameZone);
            }
            else
            {
                TryToSpawnPieceOfEnvironment(_currentPosition, ChanceToSpawnOutGameZone);
            }
        }
    }
    private bool IsSpawnPositionInGameZone()
    {
        return _currentPosition.x < StartGameZone.transform.position.x && _currentPosition.x > EndGameZone.transform.position.x;
    }

    private void TryToSpawnPieceOfEnvironment(Vector3 spawnPosition, float chance)
    {
        float randomChance = Random.Range(MinChanceToSpawn, MaxChanceToSpawn); 
        
        if(chance >= randomChance)
        {
            GameObject randomPieceOfEnvironment = StaticEnvironment[Random.Range(0,StaticEnvironment.Length)];
            Instantiate(randomPieceOfEnvironment, spawnPosition, Quaternion.identity, transform);
        }
        else
        {
            if(IsSpawnPositionInGameZone())
            {
                CoinPrefab.GetComponent<Coin>().TryToSpawnCoin(_currentPosition, gameObject.transform);
            }
        }

        _currentPosition.x -= SpawningOffset;
    }
}