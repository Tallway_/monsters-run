public static class AnimatorVariables
{
    public static class UI
    {
        public const string GameEnded = nameof(GameEnded);
        public const string GameStarted = nameof(GameStarted);
    }

    public static class Player
    {
        public const string Jump = nameof(Jump);
        public const string JumpPreparation = nameof(JumpPreparation);
        public const string DeathInIdle = nameof(DeathInIdle);
        public const string DeathInJump = nameof(DeathInJump);
        public const string DeathInWater = nameof(DeathInWater);
        public const string DeathOutOfCamera = nameof(DeathOutOfCamera);
        public const string JumpCanceled = nameof(JumpCanceled);
    }
}
