public class Layers 
{
    public const string Player = nameof(Player);
    public const string Water = nameof(Water);
    public const string UI = nameof(UI);
    public const string Obstacle = nameof(Obstacle);
    public const string Ground = nameof(Ground);
    public const string Bound = nameof(Bound);
    public const string MoveTarget = nameof(MoveTarget);
    public const string Vehicle = nameof(Vehicle);
    public const string Collectables = nameof(Collectables);
    public const string Log = nameof(Log);
}
