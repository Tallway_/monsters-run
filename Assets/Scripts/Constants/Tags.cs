public class Tags 
{
    public const string GroundLine = nameof(GroundLine);
    public const string RoadLine = nameof(RoadLine);
    public const string RiverLine = nameof(RiverLine);
    public const string RoadLineDefault = nameof(RoadLineDefault);
    public const string EndLine = nameof(EndLine);
    public const string Obstacle = nameof(Obstacle);
    public const string Cell = nameof(Cell);
    public const string Vehicle = nameof(Vehicle);
    public const string Log = nameof(Log);
    public const string StartGameZone = nameof(StartGameZone);
    public const string EndGameZone = nameof(EndGameZone);
    public const string Coin = nameof(Coin);
    public const string Player = nameof(Player);
}
