using UnityEngine;

public class MoveTarget : MonoBehaviour
{
    public BoxCollider _thisCollider;

    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.TryGetComponent(out CharacterMover characterMover))
        {
            if(characterMover.NextCollider == null)
            {
                characterMover.NextCollider = _thisCollider;
                characterMover.CurrentParentTransform = _thisCollider.transform;
                characterMover.SetParent(characterMover.NextCollider.transform);
            }
        }   
    }

}
