using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class LogBehavior : MonoBehaviour
{
    [Space, Header("Coin Prefab")] public GameObject CoinPrefab;
    [Space, Header("Coin Spawn Positions")] public List<Transform> SpawnTransforms; 

    public Vector3 StartVelocity { get; private set; }
    public Vector3 InGameZoneVelocity { get; private set; } 

    private Rigidbody _rigidbody;
    private LogPooler _logPooler;
    private Vector3 _returnPosition;

    public void Initialize(LogPooler logPooler, Vector3 returnPosition, Vector3 startVelocity, Vector3 inGameZoneVelocity)
    {
        _logPooler = logPooler;
        _returnPosition = returnPosition;
        StartVelocity = startVelocity;
        InGameZoneVelocity = inGameZoneVelocity;
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();

        TryToSpawnCoins();      
    }

    private void TryToSpawnCoins()
    {
        Coin coin = CoinPrefab.GetComponent<Coin>();

        foreach(var spawnTransform in SpawnTransforms)
        {
            coin.TryToSpawnCoin(spawnTransform.position, this.gameObject.transform);
        }
    }

    public void ReturnToPool()
    {
        _logPooler.ReturnObject(this, _returnPosition);
    }
}
