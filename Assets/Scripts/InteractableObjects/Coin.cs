using UnityEngine;

public class Coin : MonoBehaviour
{
    private const float ChanceToSpawn = 0.01f; 
    
    [SerializeField] private float _speed;

    private void Update()
    {
        transform.Rotate(Vector3.up * _speed * Time.deltaTime);
    }

    public void TryToSpawnCoin(Vector3 spawnPosition, Transform parent)
    {
        float randomChance = Random.Range(0f, 1f);

        if(ChanceToSpawn >= randomChance)
        {
            Instantiate(this.gameObject, spawnPosition + Vector3.up, Quaternion.identity, parent);
        }
    }
}
