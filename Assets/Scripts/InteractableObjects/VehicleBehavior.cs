using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class VehicleBehavior : MonoBehaviour
{
    [Header("Wheel Colliders")]
    public WheelCollider LeftForwardWheelCollider;
    public WheelCollider LeftBackwardWheelCollider; 
    public WheelCollider RightForwardWheelCollider;
    public WheelCollider RightBackwardWheelCollider;

    [Space, Header("Wheel Transforms")]
    public Transform LeftForwardWheel;
    public Transform LeftBackwardWheel;
    public Transform RightForwardWheel;
    public Transform RightBackwardWheel;

    private const float MinMotorForce = 100f;
    private const float MaxMotorForce = 130f;

    private Rigidbody _rigidbody;
    private float _motorForce;

    private VehiclePooler _vehiclePooler;
    private Vector3 _returnPosition;

    public void Init(VehiclePooler vehiclePooler, Vector3 returnPosition)
    {
        _vehiclePooler = vehiclePooler;
        _returnPosition = returnPosition;
    }

    private void Awake() 
    {   
        _rigidbody = GetComponent<Rigidbody>();

        _motorForce = Random.Range(MinMotorForce, MaxMotorForce);        
    }

    private void FixedUpdate()
    {
        UpdatePosition(); 
        SetAcceleration();        
    }

    private void SetAcceleration()
    {
        LeftForwardWheelCollider.motorTorque = _motorForce;
        RightForwardWheelCollider.motorTorque = _motorForce;
    }

    private void UpdatePosition()
    {
        UpdatePosition(LeftForwardWheelCollider, LeftForwardWheel);
        UpdatePosition(RightForwardWheelCollider, RightForwardWheel);
        UpdatePosition(LeftBackwardWheelCollider, LeftBackwardWheel);
        UpdatePosition(RightBackwardWheelCollider, RightBackwardWheel);
    }

    private void UpdatePosition(WheelCollider collider, Transform wheel)
    {
        Vector3 position = transform.position;
        Quaternion rotation = transform.rotation;

        collider.GetWorldPose(out position, out rotation);

        wheel.position = position;
        wheel.rotation = rotation;
    }

    public void ReturnToPool()
    {
        _vehiclePooler.ReturnObject(this, _returnPosition);
    }

    private void OnCollisionEnter(Collision other) 
    {
        if(other.gameObject.TryGetComponent(out CharacterBehavior character))
        {
            List<float> zPoints = new List<float>();

            for(int i = 0; i < other.contacts.Length; i++)
            {
                zPoints.Add(other.contacts[i].point.z);
            }
           
            float pointsNumber = zPoints.Distinct().Count();;
            if(pointsNumber > 1)
            {
                
                character.PlayDeathInIdleAnimation();
            }
            else
            {
                character.PlayDeathInJumpAnimation();
            }

            character.PlayBumpingEffect();
            character.gameObject.layer = 0;
            character.IsDead = true;
            
            AudioPlayer.Instance.AudioSource.PlayOneShot(AudioPlayer.Instance.DeathByCarCollision[Random.Range(0,AudioPlayer.Instance.DeathByCarCollision.Length)]);
        }
    }
}
