using System;
using UnityEngine;

[RequireComponent(typeof(CharacterRotator), typeof(CharacterMover), (typeof(CharacterDirection)))]
public class CharacterBehavior : MonoBehaviour
{
    #region Constants
    private const float PauseTime = 0.1f;
    private const float RayDistance = 2f;
    #endregion

    public static event Action CoinPickingUp;
    public bool IsDead { get; set; }
    public int LinesPassedCount { get; set; }

    [SerializeField] private AudioClip _jumpSound;
    [SerializeField] private AudioClip _pickUpCoinSound;
    [SerializeField] private Animator _animator;
    [SerializeField] private ParticleSystem _bumping, _splashingWater, _coinAddition;
    private Rigidbody _rigidbody;
    private AudioSource _audioSource;

    private void OnEnable() 
    {
        CharacterMover.LineChanged += OnLineChanging;
    }

    private void OnDisable() 
    {
        CharacterMover.LineChanged -= OnLineChanging; 
    }

    private void OnLineChanging(int value)
    {
        LinesPassedCount += value;
    }  

    private void Start()
    {
        Initialize();
        Invoke(nameof(CheckObstacleAtPlayerPosition), PauseTime);
    }

    private void Initialize()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _audioSource = GetComponent<AudioSource>();

        LinesPassedCount = 0;
        IsDead = false;
    }

    private void CheckObstacleAtPlayerPosition()
    {
        bool isObstacle = Physics.Raycast(transform.position + new Vector3(-2f, 0.5f, 0f), Vector3.right, out RaycastHit hit, RayDistance, LayerMask.GetMask(Layers.Obstacle));
        if(isObstacle)
        {
            Destroy(hit.transform.gameObject);
        }
    }

    private void OnCollisionEnter(Collision other) 
    { 
        if(other.gameObject.TryGetComponent(out Coin coin))
        {
            CoinPickingUp?.Invoke();
            Instantiate(_coinAddition, other.transform.position, Quaternion.identity);
            _audioSource.PlayOneShot(_pickUpCoinSound);
            Destroy(coin.gameObject);
        }
    }

    public void PlayJumpSound()
    {
        _audioSource.PlayOneShot(_jumpSound);
    }

    public void PlayDeathInIdleAnimation()
    {
        _animator.SetTrigger(AnimatorVariables.Player.DeathInIdle);
    }

    public void PlayDeathInJumpAnimation()
    {
        _animator.SetTrigger(AnimatorVariables.Player.DeathInJump);
    }

    public void PlayDeathInWaterAnimation()
    {
        _animator.SetTrigger(AnimatorVariables.Player.DeathInWater);
    }

    public void PlayDeathOutOfCameraAnimation()
    {
        _animator.SetTrigger(AnimatorVariables.Player.DeathOutOfCamera);
    }

    public void PlayBumpingEffect()
    {
        Instantiate(_bumping, transform.position + Vector3.up * 0.5f, Quaternion.identity);
    }

    public void PlaySplashingWaterEffect()
    {
        Instantiate(_splashingWater, transform.position, Quaternion.identity);
    }
}
