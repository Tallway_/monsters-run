using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRotator : MonoBehaviour
{
    private const float TurnSpeed = 12f;
    private Dictionary<Vector3, Quaternion> _rotations;

    private void OnEnable() => CharacterDirection.DirectionChanged += OnDirectionChanged;
    private void OnDisable() => CharacterDirection.DirectionChanged -= OnDirectionChanged;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _rotations = new Dictionary<Vector3, Quaternion>();
        _rotations[Vector3.forward] = Quaternion.Euler(0,0,0);
        _rotations[-Vector3.forward] = Quaternion.Euler(0,180,0);
        _rotations[-Vector3.right] = Quaternion.Euler(0,-90,0);
        _rotations[Vector3.right] = Quaternion.Euler(0,90,0);         
    }
   
    private void OnDirectionChanged()
    {
        StopAllCoroutines();
        StartCoroutine(Rotate());
    }

    private IEnumerator Rotate()
    {
        Quaternion _currentRotation = transform.rotation;

        for(float i = 0; i < 1; i += Time.deltaTime * TurnSpeed)
        {
            transform.rotation = Quaternion.Lerp(_currentRotation, _rotations[CharacterDirection.LookDirection], i);
            
            yield return null;
        }

        transform.rotation = Quaternion.Lerp(_currentRotation, _rotations[CharacterDirection.LookDirection], 1f);
    }
}
