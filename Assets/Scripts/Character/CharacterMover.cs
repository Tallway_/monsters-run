using System;
using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource), typeof(BoxCollider))]
public class CharacterMover : MonoBehaviour
{
    #region Constants
    private const float SphereRadiusForGround = 0.1f;
    private const float SphereRadiusForMove = 0.5f;
    private const float JumpSpeedAcceleration = 8f;
    private const float CheckDistance = 2f;
    private const float CheckVehicleDistance = 0.2f;
    private const float JumpHeight = 1f;
    private const float JumpDistance = 2f;
    private const int First = 0;
    #endregion

    public static event Action<int> LineChanged;
    public static event Action<float> XPositionChanged;
    public bool IsAbleToMove { get; set; }
    public Collider NextCollider { get; set; }
    public Transform CurrentParentTransform { get; set; }

    [SerializeField] private BoxCollider _collider;
    [SerializeField] private CharacterDirection _characterDirection;
    [SerializeField] private CharacterBehavior _characterBehavior;

    private Animator _animator;
    private Touch _touch;
    private RaycastHit _groundHit, _cellHit;
    private Vector3 _startPosition, _middlePosition, _targetPosition, _previousPosition, _nextPosition, _raycastingStartPosition;
    private bool _isJumpEnded, _isJumpPrepared, _isGrounded, _isCellForward;
    private float _distanceToFeet;
    private Func<float, float> _inverseCosinus, _sinus;

    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        _animator = GetComponentInChildren<Animator>();
        _collider = GetComponent<BoxCollider>();
        
        _isJumpEnded = true;   

        _raycastingStartPosition = transform.position + _collider.center;
        _distanceToFeet = _collider.center.y;  

        _inverseCosinus = x => 1 - Mathf.Cos((x * Mathf.PI) / 2);
        _sinus = x => Mathf.Sin((x * Mathf.PI) / 2);   
    }

    private void Update() 
    {
        CheckXPosition();

        if(IsAbleToMove)
        {
            _raycastingStartPosition = transform.position + _collider.center;

            _isGrounded = Physics.SphereCast(_raycastingStartPosition, 
                                            SphereRadiusForGround, 
                                            -transform.up, 
                                            out _groundHit, 
                                            _distanceToFeet, 
                                            LayerMask.GetMask(Layers.Ground, Layers.Log));

            if(_isGrounded && _isJumpEnded)
            {
                #if UNITY_STANDALONE_WIN || UNITY_EDITOR
                if(Input.GetKeyDown(KeyCode.W)) { PrepareJump(); }
                if(Input.GetKeyDown(KeyCode.A)) { PrepareJump(); }
                if(Input.GetKeyDown(KeyCode.S)) { PrepareJump(); }
                if(Input.GetKeyDown(KeyCode.D)) { PrepareJump(); }

                if(_isJumpPrepared)
                {
                    if(Input.GetKeyUp(KeyCode.W)) { StartCoroutine(Jump()); }
                    if(Input.GetKeyUp(KeyCode.A)) { StartCoroutine(Jump()); }
                    if(Input.GetKeyUp(KeyCode.S)) { StartCoroutine(Jump()); }
                    if(Input.GetKeyUp(KeyCode.D)) { StartCoroutine(Jump()); }
                }
                #endif

                _characterDirection.IsAbleToSetDirection = true;

                if(Input.touchCount > 0)
                {
                    _touch = Input.GetTouch(First);

                    if(_touch.phase == TouchPhase.Began)
                    {
                        PrepareJump();
                    }

                    if(_touch.phase == TouchPhase.Ended)
                    {
                        if(_isJumpPrepared)
                        {
                            StartCoroutine(Jump());
                        }
                    } 
                }
            }
            else
            {
                _characterDirection.IsAbleToSetDirection = false;
            }
        }

        _nextPosition = transform.position;
    }

    private void PrepareJump()
    { 
        _animator.SetTrigger(AnimatorVariables.Player.JumpPreparation);
        _isJumpPrepared = true;
    }

    public IEnumerator Jump()
    {
        bool canJump = CheckJumpCapability();
        if(canJump)
        {
            _animator.SetTrigger(AnimatorVariables.Player.JumpCanceled);
            _isJumpPrepared = false;
            yield break;
        }

        InitializeJumpVariables();

        _characterBehavior.PlayJumpSound();
        yield return MoveFrom(_startPosition, true, _inverseCosinus);
        yield return MoveFrom(_middlePosition, false, _sinus);

        ReassignColliderReference();

        CheckIsLineChanged();

        _animator.ResetTrigger(AnimatorVariables.Player.Jump);
        //_isJumpEnded = true;       
    }

    private bool CheckJumpCapability()
    {
        RaycastHit obstacleHit = default;
        bool isObstacle = Physics.Raycast(transform.position + Vector3.up * 0.5f, 
                                          CharacterDirection.LookDirection, 
                                          out obstacleHit, 
                                          CheckDistance, 
                                          LayerMask.GetMask(Layers.Obstacle, Layers.Bound));

        return isObstacle;
    }

    private void InitializeJumpVariables()
    {
        _isJumpEnded = false;
        _isJumpPrepared = false;
        _isJumpEnded = true; 
        _startPosition = transform.position;
        _animator.SetTrigger(AnimatorVariables.Player.Jump);
        _animator.ResetTrigger(AnimatorVariables.Player.JumpPreparation);

        CheckIsMoveTargetForward();

        TryGetTargetPosition();
    }

    private void TryGetTargetPosition()
    {
        if(NextCollider != null)
        {           
            _targetPosition = new Vector3(NextCollider.bounds.center.x,  NextCollider.bounds.min.y, NextCollider.bounds.center.z);
        }
        else
        {
            _targetPosition = _startPosition + CharacterDirection.LookDirection * JumpDistance - Vector3.up;
        }
    }

    public void SetParent(Transform parent)
    {
        gameObject.transform.SetParent(parent);
    }

    private void CheckIsMoveTargetForward()
    {
        _isCellForward = Physics.SphereCast(transform.position,
                                            SphereRadiusForMove,
                                            CharacterDirection.LookDirection,
                                            out _cellHit,
                                            CheckDistance,
                                            LayerMask.GetMask(Layers.MoveTarget));
        
        NextCollider = _cellHit.collider;

        if(NextCollider != null)
        {
            if(NextCollider.transform != CurrentParentTransform)
            {
                CurrentParentTransform = NextCollider.transform;
                SetParent(NextCollider.transform);
            }
        }
    }

    private IEnumerator MoveFrom(Vector3 startPosition, bool wouldMiddlePositionChanged, Func<float, float> easing)
    {
        Vector3 finishPosition = Vector3.zero;
       
        for(float i = 0; i < 1; i += Time.deltaTime * JumpSpeedAcceleration)
        {
            if (wouldMiddlePositionChanged)
            {
                Vector3 halfDifference = (_targetPosition - _startPosition) / 2f;
                _middlePosition = _startPosition + halfDifference + Vector3.up * JumpHeight;
                finishPosition = _middlePosition;
            }
            else
            {
                finishPosition = _targetPosition;
            }

            transform.position = Vector3.Lerp(startPosition, finishPosition, easing(i));

            TryGetTargetPosition();

            yield return null;
        }

        transform.position = _targetPosition;
    }

    private void CheckXPosition()
    {
        Vector3 difference = _nextPosition - _previousPosition;
        if (difference.x != 0f)
        {
            XPositionChanged?.Invoke(difference.x);
            _previousPosition = _nextPosition;
        }
    }

    private void ReassignColliderReference()
    {
        NextCollider = null;
    }

    private void CheckIsLineChanged()
    {
        float passedDistance = _targetPosition.z - _startPosition.z;
        bool isZPositionChaged = passedDistance != 0;

        if(isZPositionChaged)
        {
            int roundValue = Mathf.RoundToInt(passedDistance / 2);
            LineChanged?.Invoke(roundValue);
        }
    }
}


