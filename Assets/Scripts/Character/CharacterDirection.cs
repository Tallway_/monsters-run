using System;
using UnityEngine;

public enum Direction
{
    Forward,
    Backward,
    Left,
    Right
} 

public class CharacterDirection : MonoBehaviour
{
    #region Constants
    private const float TouchBounds = 100f; 
    private const int First = 0;
    #endregion

    public static event Action DirectionChanged; 
    public static Vector3 LookDirection { get; private set; } 
    public bool IsAbleToSetDirection { get; set; }

    private Touch _touch;
    private Vector2 _startTouchPosition, _endTouchPostion;

    private void OnEnable() 
    {
        GameController.Instance.GameEnding += OnGameEnding;        
    }

    private void OnDisable() 
    {
        GameController.Instance.GameEnding -= OnGameEnding;
    }

    private void OnGameEnding()
    {
        IsAbleToSetDirection = false;
    }

    private void Awake() 
    {
        Set(Direction.Forward);
    }
    
    private void Update() 
    {
        if(IsAbleToSetDirection)
        {
            #if UNITY_STANDALONE_WIN || UNITY_EDITOR
            if(Input.GetKeyDown(KeyCode.A)) { ChangeDirection(Direction.Left); }
            if(Input.GetKeyDown(KeyCode.S)) { ChangeDirection(Direction.Backward); }
            if(Input.GetKeyDown(KeyCode.W)) { ChangeDirection(Direction.Forward); }
            if(Input.GetKeyDown(KeyCode.D)) { ChangeDirection(Direction.Right); }    
            #endif

            if(Input.touchCount > 0)
            {
                _touch = Input.GetTouch(First);
                
                if(_touch.phase == TouchPhase.Began)
                {
                    _startTouchPosition = _touch.position;
                }

                if(_touch.phase == TouchPhase.Ended)
                {
                    _endTouchPostion = _touch.position;

                    Vector2 deltaPosition = _endTouchPostion - _startTouchPosition;
                    float absDeltaXPosition = Mathf.Abs(deltaPosition.x);
                    float absDeltaYPosition = Mathf.Abs(deltaPosition.y);

                    if(absDeltaXPosition <= TouchBounds && absDeltaYPosition <= TouchBounds)
                    {
                        ChangeDirection(Direction.Forward);
                    }
                    else if(absDeltaXPosition > absDeltaYPosition)
                    {
                        if (deltaPosition.x > 0)
                        {
                            ChangeDirection(Direction.Right);
                        }
                        else
                        {
                            ChangeDirection(Direction.Left);
                        }
                    }
                    else 
                    {
                        if (deltaPosition.y > 0)
                        {
                            ChangeDirection(Direction.Forward);
                        }
                        else
                        {
                            ChangeDirection(Direction.Backward);
                        }
                    }
                }
            }  
        } 
    }

    private void ChangeDirection(Direction direction)
    {
        Set(direction);
        DirectionChanged?.Invoke();
    }

    private void Set(Direction direction)
    {
        switch(direction)
        {
            case Direction.Forward:
                LookDirection = Vector3.forward;
                break;
            case Direction.Backward:
                LookDirection = -Vector3.forward;
                break;
            case Direction.Left:
                LookDirection = -Vector3.right;
                break;
            case Direction.Right:
                LookDirection = Vector3.right;
                break;
        }     
    }
}
