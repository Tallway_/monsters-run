public class Data
{
    public int MaxLinesPassed { get; set; }
    public int CollectedCoins { get; set; }
    public int SoundSwitch { get; set; }
    public int MusicSwitch { get; set; }
    public int LockSwitch { get; set; }
    public int QualitySwitch { get; set; }

    public Data() {}
}
