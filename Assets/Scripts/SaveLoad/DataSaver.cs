using UnityEngine;

public class DataSaver
{
    private Data _gameData = null;

    public DataSaver(Data data)
    {
        _gameData = data;
    }

    public void Save()
    {
        PlayerPrefs.SetInt(nameof(MaxLinesPassed), _gameData.MaxLinesPassed);
        PlayerPrefs.SetInt(nameof(CoinsCount), _gameData.CollectedCoins);
        PlayerPrefs.SetInt(nameof(SoundSwitch), _gameData.SoundSwitch);
        PlayerPrefs.SetInt(nameof(MusicSwitch), _gameData.MusicSwitch);
        PlayerPrefs.SetInt(nameof(LockSwitch), _gameData.LockSwitch);
        PlayerPrefs.SetInt(nameof(QualitySwitch), _gameData.QualitySwitch);
    }
}
