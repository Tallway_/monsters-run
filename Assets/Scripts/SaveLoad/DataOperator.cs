using UnityEngine;

public class DataOperator : MonoBehaviour
{
    [SerializeField] private MaxLinesPassed _maxLinesPassed;
    [SerializeField] private CoinsCount _coinsCount;
    [SerializeField] private SoundSwitch _soundSwitch;
    [SerializeField] private MusicSwitch _musicSwitch;
    [SerializeField] private LockSwitch _lockSwitch;
    [SerializeField] private QualitySwitch _qualitySwitch;

    private Data _data = null;

    private void OnEnable() 
    {
        GameController.Instance.GameEnding += OnGameEnding;
        LoadData();
    }
    private void OnDisable() 
    {
        GameController.Instance.GameEnding -= OnGameEnding;
        SaveData();
    }

    private void OnGameEnding()
    {
        SaveData();
    }

    private void SetGameData()
    {
        _maxLinesPassed.SetValue(_data.MaxLinesPassed);
        _coinsCount.SetValue(_data.CollectedCoins);
        _soundSwitch.SetOption(_data.SoundSwitch);
        _musicSwitch.SetOption(_data.MusicSwitch);
        _lockSwitch.SetOption(_data.LockSwitch);
        _qualitySwitch.SetOption(_data.QualitySwitch);
        
    }

    private Data GetGameData()
    {
        Data data = new Data();

        data.MaxLinesPassed = _maxLinesPassed.GetValue();
        data.CollectedCoins = _coinsCount.GetValue();
        data.SoundSwitch = _soundSwitch.GetOption();
        data.MusicSwitch = _musicSwitch.GetOption();
        data.LockSwitch = _lockSwitch.GetOption();
        data.QualitySwitch = _qualitySwitch.GetOption();

        return data;
    }

    public void SaveData()
    {
        _data = GetGameData();

        DataSaver dataSaver = new DataSaver(_data);
        dataSaver.Save();
    }

    private void LoadData() 
    {   
        DataLoader dataLoader = new DataLoader();
        _data = dataLoader.Load();

        SetGameData();
    }
}
