using UnityEngine;

public class DataLoader
{
    private Data _gameData = null;

    public DataLoader()  
    { 
        _gameData = new Data(); 
    }

    public Data Load()
    {
        _gameData.MaxLinesPassed = PlayerPrefs.GetInt(nameof(MaxLinesPassed), 0);
        _gameData.CollectedCoins = PlayerPrefs.GetInt(nameof(CoinsCount), 0);
        _gameData.SoundSwitch = PlayerPrefs.GetInt(nameof(SoundSwitch), 1);
        _gameData.MusicSwitch = PlayerPrefs.GetInt(nameof(MusicSwitch), 1);
        _gameData.LockSwitch = PlayerPrefs.GetInt(nameof(LockSwitch), 1);
        _gameData.QualitySwitch = PlayerPrefs.GetInt(nameof(QualitySwitch), 1);

        return _gameData;
    }
}
