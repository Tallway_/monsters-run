using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Horn : MonoBehaviour
{
    #region  Constants   
    private const float Delay = 2f;
    private const float RepeatingRate = 5f;
    private const float ChanceToPlaySound = 0.02f; 
    #endregion

    [SerializeField] private List<AudioClip> _hornSounds;
    [SerializeField] private AudioSource _audioSource;
    private AudioClip _hornSound;

    private void OnEnable() 
    {
        InvokeRepeating(nameof(TryToPlaySound), Delay, RepeatingRate);        
    }

    private void Start()
    {
        SetHornSound();        
    }

    private void SetHornSound()
    {
        int randomSound = Random.Range(0,_hornSounds.Count);
        _hornSound = _hornSounds[randomSound];
    }

    private void TryToPlaySound()
    {
        float randomChance = Random.Range(0f, 1f);
        if (ChanceToPlaySound >= randomChance)
        {
            _audioSource.PlayOneShot(_hornSound);
        }       
    }

    private void OnDisable() 
    {
        CancelInvoke(nameof(TryToPlaySound));
    }
}
