using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioPlayer : MonoBehaviour
{
    public static AudioPlayer Instance { get { return _instance; } }
    private static AudioPlayer _instance;
    private CharacterBehavior _character;
    public AudioSource AudioSource;
    public AudioClip DeathInIdleSound, DeathInRiverSound, DeathInBoundSound;
    [Space] public AudioClip[] DeathByCarCollision;

    private void Awake() 
    {
        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } 
        else 
        {
            _instance = this;
        }

        DontDestroyOnLoad(gameObject);
    }
    
    private void Update()
    {
        if(_character != null)
        {
            transform.position = _character.transform.position;      
        }
        else
        {
            _character = FindObjectOfType<CharacterBehavior>();
        }
    }
}
